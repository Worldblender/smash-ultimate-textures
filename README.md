A repository holding PNG textures from the Nintendo Switch game Super Smash Bros. Ultimate. DDS files have been generated as part of this repository, but they are not uploaded here in order to save disk space.

Original project thread: <https://www.vg-resource.com/thread-34836.html>

The original model and texture files were uploaded by Random Talking Bush and can be found at: <https://mega.nz/#F!AAwWzCjT!Zd5kKpuQcE647DSRtpFShw>

The NUTEXB to DDS script, written by by Random Talking Bush, and which is used as part of this repository, can be downloaded at <https://mega.nz/#!LlZDwCAR!_nK-D-dvuYpISdyqQYWhALxydxGrmEaSm0Wc3qAHby0>

This repository uses Git Large File Storage (LFS), in order to keep the repository sizes more manageable. See https://git-lfs.github.com/ for more information and how to get the client.

The following software has helped me in conversion of the textures, listed by the order they are used: (now fully GNU/Linux native!):

1. [QuickBMS](http://aluigi.altervista.org/quickbms.htm) - a multiplatform extractor engine that can be programmed through some simple instructions contained in textual scripts, it's intended for extracting files and information from the archives of any software and, moreover, games. The native Linux 32-bit x86 binaries, which are used for this repository, can be downloaded from <http://aluigi.altervista.org/papers/quickbms_linux.zip>

2. [Compressonator](https://gpuopen.com/compressonator/) - A tool suite from AMD (Advanced Micro Devices) for texture and 3d model compression, optimization and analysis using CPUs, GPUs and APUs. Git repository: <https://github.com/GPUOpen-Tools/Compressonator>

3. For textures of sizes of 16x16 and 8x8: A custom-made program to convert multiple textures at once, written by me and KillzXGaming: <https://github.com/RQWorldblender/TextureConverter> | Upstream repository: <https://github.com/KillzXGaming/TextureConverter>
