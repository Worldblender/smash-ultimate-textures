#!/bin/sh
# Based from https://askubuntu.com/questions/590837/how-to-batch-compress-folders
work_dir=stage/
format=7z
for folder in $work_dir*/
do
  7z a -mx7 -mmt "${folder%/}.${format}" "$folder"
done
